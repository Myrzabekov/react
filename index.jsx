import React, {Component} from "react";
import ReactDOM, { render } from "react-dom";
import "./styles.css";


// class App extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             message: 'Hello world',
//             showMessage: false
//         }   
//     };

//     showMessage(status) {
//         this.setState({showMessage: status})
//     }

//     render() {
//         return (
//             <div className="App">
//                 <button onClick = {() => this.showMessage(true)}>Вызвать слово</button>
//                 {this.state.showMessage ? <p>{this.state.message}</p> : ""}
//             </div>
//         )
//     }
// };

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            color: "black",
            isHide: false
    };

}
    render() {
        return (
            <div className="App">
                <button onClick = {() => this.setState({color: 'red'})}>Красный</button>
                <button onClick = {() => this.setState({color: 'green'})}>Зеленый</button>
                <button onClick = {() => this.setState({color: 'blue'})}>Синий</button>
                <button onClick = {() => this.setState({isHide: true})}>Скрыть</button>
                <button onClick = {() => this.setState({isHide: false})}>Вернуть</button>

                <div className={`box ${this.state.isHide ? 'box--hide' : ""}`} style={{ backgroundColor: this.state.color }}/>
            </div>
            );
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('root')
)


/*

Каждая кнопка по клику должна окрашить box в свой цвет
Кнопка "Скрыть" по клику должна box'у применить класс box--hide
*/



